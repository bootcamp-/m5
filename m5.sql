-- insert/update/delete CUSTOMER

INSERT INTO public.customer
(store_id, first_name, last_name, email, address_id, activebool, create_date, last_update, active)
VALUES(0, '', '', '', 1, true, 'now'::text::date, now(), 0);

UPDATE public.customer
SET store_id=0, first_name='', last_name='', email='', address_id=0, activebool=true, create_date='now'::text::date, last_update=now(), active=0
WHERE customer_id=nextval('customer_customer_id_seq'::regclass);

DELETE FROM public.customer
WHERE customer_id=nextval('customer_customer_id_seq'::regclass);

-- insert/update/delete STAFF

INSERT INTO public.staff
(first_name, last_name, address_id, email, store_id, active, username, "password", picture)
VALUES('', '', 1, '', 0, true, '', '', null);

UPDATE public.staff
SET first_name='', last_name='', address_id=0, email='', store_id=0, active=true, username='', "password"='', picture=?
WHERE staff_id=nextval('staff_staff_id_seq'::regclass);

DELETE FROM public.staff
WHERE staff_id=nextval('staff_staff_id_seq'::regclass);

-- insert/update/delete ACTOR

INSERT INTO public.actor
(first_name, last_name)
VALUES('', '');

UPDATE public.actor
SET first_name='', last_name=''
WHERE actor_id=nextval('actor_actor_id_seq'::regclass);

DELETE FROM public.actor
WHERE actor_id=nextval('actor_actor_id_seq'::regclass);

-- listar * rental con datos customer según año y mes (ej: 07/2005)

SELECT *
FROM public.rental
INNER JOIN public.customer
ON public.rental.customer_id = public.customer.customer_id
WHERE To_char(rental_date, 'YYYY') = '2005' AND To_char(rental_date, 'MM') = '07';

-- listar numero (payment_id) fecha (payment_date) y total (amount) de * payment

SELECT payment_id AS numero, Date(payment_date) AS fecha, amount AS total
FROM public.payment
GROUP BY Date(payment_date), payment_id
ORDER BY payment_id;

-- listar * film de 2006 con rental_rate > 4.0

SELECT *
FROM public.film
WHERE release_year = 2006 AND rental_rate > 4.0
ORDER BY film_id;

-- hacer diccionario que contenga nombre de tablas y columnas, si es nullable y data type

SELECT table_name, column_name, is_nullable, data_type
FROM information_schema.columns
WHERE table_schema = 'public'
ORDER BY table_name, column_name;